import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("iocs")


DEPLOYED_IOCSLUG = "foo"
UNDEPLOYED_IOCSLUG = "baz"


def test_ioc_is_running(host):
    service = host.service(f"ioc-{DEPLOYED_IOCSLUG}")
    assert service.is_enabled
    assert service.is_running

    cmd = host.run("/tmp/caget -w 5 -t foo:BaseVersion")
    assert cmd.rc == 0
    assert cmd.stdout.startswith("7.0.7")


def test_undeployed_ioc_service_removed(host):
    opt_iocs = "/opt/iocs"
    opt_nonv = "/opt/nonvolatile"

    assert not host.file(os.path.join(opt_iocs, UNDEPLOYED_IOCSLUG)).exists
    assert not host.file(os.path.join(opt_nonv, UNDEPLOYED_IOCSLUG)).exists


def test_undeployed_ioc_procserv_log_remains(host):
    assert host.file(f"/var/log/procServ/out-{UNDEPLOYED_IOCSLUG}.log").is_file


def test_undeployed_ioc_no_longer_running(host):
    service = host.service(f"ioc-{UNDEPLOYED_IOCSLUG}")
    assert not service.is_enabled
    assert not service.is_running
