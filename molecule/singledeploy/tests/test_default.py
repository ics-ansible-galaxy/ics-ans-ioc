import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("iocs")


def test_ioc_is_running(host):
    deployed_iocname = "foo"
    service = host.service(f"ioc-{deployed_iocname}")
    assert service.is_enabled
    assert service.is_running

    cmd = host.run(f"/tmp/caget -w 5 -t {deployed_iocname}:BaseVersion")
    assert cmd.rc == 0
    assert cmd.stdout.startswith("7.0.7")


def test_extra_vars_ioc_was_deployed(host):
    host.file("/etc/motd").contains("foo-extra-vars")


def test_ioc_is_not_installed_or_running(host):
    non_deployed_ioc = "baz"
    service = host.service(f"ioc-{non_deployed_ioc}")
    assert not service.is_enabled
    assert not service.is_running

    assert not host.file(os.path.join("/opt/iocs", non_deployed_ioc)).exists
