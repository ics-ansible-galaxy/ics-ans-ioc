import os

import pytest
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("iocs")

UNDEPLOYED_IOCSLUG = "qux"


def test_procserv_installed(host):
    cmd = host.run("/usr/bin/procServ --version")
    assert cmd.rc == 0
    assert cmd.stdout.startswith("procServ Process Server")


def test_conda_installed(host):
    cmd = host.run("/opt/conda/bin/conda --version")
    assert cmd.rc == 0
    assert cmd.stdout.startswith("conda")


def test_syslog_ng_confs(host):
    assert host.file("/etc/syslog-ng/conf.d/conserver.conf").exists
    assert host.file("/etc/syslog-ng/conf.d/procserv.conf").exists
    assert host.file("/etc/syslog-ng/conf.d/ioclog.conf").exists
    assert host.file("/etc/syslog-ng/conf.d/iocputlog.conf").exists


@pytest.mark.parametrize("iocslug", ["foo", "baz"])
def test_ioc_is_running(host, iocslug: str):
    service = host.service(f"ioc-{iocslug}")
    assert service.is_enabled
    assert service.is_running

    cmd = host.run("/tmp/caget -w 5 -t foo:BaseVersion")
    assert cmd.rc == 0
    assert cmd.stdout.startswith("7.0.7")


def test_undeployed_ioc_service_removed(host):
    opt_iocs = "/opt/iocs"
    opt_nonv = "/opt/nonvolatile"

    assert not host.file(os.path.join(opt_iocs, UNDEPLOYED_IOCSLUG)).exists
    assert not host.file(os.path.join(opt_nonv, UNDEPLOYED_IOCSLUG)).exists


def test_undeployed_ioc_procserv_log_remains(host):
    assert host.file(f"/var/log/procServ/out-{UNDEPLOYED_IOCSLUG}.log").is_file


def test_undeployed_ioc_no_longer_running(host):
    service = host.service(f"ioc-{UNDEPLOYED_IOCSLUG}")
    assert not service.is_enabled
    assert not service.is_running
